## Test Android Edit Text

__Descripción__:

- Este proyecto es prueba simple para verificar el comportamiento del EditText de forma que se pueda habilitar y deshabitar las sugerencias al escribir.

- La aplicación muestra dos campos "EditText". El primero de ellos es un EditText estándar. El segundo es un EditText que no muestra sugerencias al escribir, utilizando la propiedad:
	android:inputType="textVisiblePassword"

[Descarga del APK de ejemplo](https://bitbucket.org/jorgejiro/testandroidedittext/src/ab3544a38ec4446de96d59a87d2301a2c059c82c/TestAndroidEditText.apk?at=master)